module.exports = {
  new: "new",
  acceptedByCustomer: "acceptedByCustomer",
  rejectedByCustomer: "rejectedByCustomer",
  rejectedByWorker: "rejectedByWorker",
};
